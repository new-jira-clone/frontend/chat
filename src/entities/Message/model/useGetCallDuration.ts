import { computed } from 'vue';
import { IMessage } from '@/shared';
import { convertMilliseconds } from '@new-jira-clone/libs/src/utils/time/get-time-from-ms';

export default (message: IMessage) => {
  const callDuration = computed(() => {
    if (!message.extra) return '';

    const { seconds, minutes, hours } = convertMilliseconds(Number(message.extra));

    let duration = '';

    if (hours) {
      duration += `${hours}h `;
    }

    if (minutes) {
      duration += `${minutes}m `;
    }

    if (seconds) {
      duration += `${seconds}s`;
    }

    if (duration === '') {
      duration += `${Number(message.extra)} ms`;
    }

    return duration;
  });

  return { callDuration };
};
