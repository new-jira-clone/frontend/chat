export const getShortContentFromMessage = (message: string) => message.slice(0, 30);
