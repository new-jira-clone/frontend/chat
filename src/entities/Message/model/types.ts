export interface ICleanUpMessageTypingFunction {
  clearFunction: ReturnType<typeof setTimeout>;
}
