import { IMessage, useChatStore } from '@/shared';

export const useMessageDetailsService = () => {
  const { getChatById } = useChatStore();

  const findMessageById = (chatId: string, messageId: number): IMessage | undefined => {
    const chat = getChatById(chatId);

    if (chat === undefined) return undefined;

    return chat.messages?.items.find((message) => message.id === messageId);
  };

  const setEditStatus = async (chatId: string, messageId: number, isEdit: boolean) => {
    const message = findMessageById(chatId, messageId);

    if (message === undefined) return;

    message.details.isEdit = isEdit;
  };

  const setToolbarEditStatus = async (
    chatId: string,
    messageId: number,
    isEditToolbar: boolean,
  ) => {
    const message = findMessageById(chatId, messageId);

    if (message === undefined) return;

    message.details.isEditToolbar = isEditToolbar;
  };

  const setShowingReplyInEditMessage = async (
    chatId: string,
    messageId: number,
    isShowReply: boolean,
  ) => {
    const message = findMessageById(chatId, messageId);

    if (message === undefined) return;

    message.details.isShowReplyInEditMessage = isShowReply;
  };

  const setEditMessageValue = async (
    chatId: string,
    messageId: number,
    editMessageValue: string,
  ) => {
    const message = findMessageById(chatId, messageId);

    if (message === undefined) return;

    message.details.editMessageValue = editMessageValue;
  };

  return {
    setEditStatus,
    setShowingReplyInEditMessage,
    setEditMessageValue,
    setToolbarEditStatus,
  };
};
