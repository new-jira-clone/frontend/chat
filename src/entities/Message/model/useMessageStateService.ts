import { storeToRefs } from 'pinia';
import { IMessage, INewMessage } from '@/shared';
import { useChatStore } from '@/shared/model/store/useChatStore';

export const useMessageStateService = () => {
  const { state } = storeToRefs(useChatStore());

  const addTemporaryMessage = (chatId: string, temporaryMessage: IMessage): IMessage | null => {
    const messageChat = state.value.chats.find((chat) => chat.id === chatId);

    if (messageChat === undefined) return null;

    messageChat.lastMessage = temporaryMessage;

    if (messageChat.messages) {
      messageChat.messages.items.push(temporaryMessage);
    }

    return temporaryMessage;
  };

  const onReciveNewMessage = (newMessage: INewMessage): void => {
    const messageChat = state.value.chats.find((chat) => chat.id === newMessage.chat.id);

    if (!messageChat) {
      // TODO: add to Sentry
      return;
    }

    messageChat.lastMessage = newMessage;

    if (messageChat.messages) {
      messageChat.messages.items.push(newMessage);
    }
  };

  const confirmMessage = (newMessage: INewMessage, temporaryMessageId: number): void => {
    const messageChat = state.value.chats.find(
      (chat) => chat.id === newMessage.chat.id,
    );

    if (messageChat === undefined) return;
    if (messageChat.messages === null) return;

    const notConfirmedMessage = messageChat.messages.items.find(
      (message) => message.id === temporaryMessageId,
    );

    if (notConfirmedMessage === undefined) return;

    Object.assign(notConfirmedMessage, newMessage);
  };

  const insertMessageByCreatedAt = (newMessage: IMessage, chatId: string): void => {
    const messageChat = state.value.chats.find((chat) => chat.id === chatId);

    if (messageChat === undefined || messageChat.messages === null) return;

    const lastMessage = messageChat.lastMessage;
    const isNewMessageCreatedLaster = new Date(newMessage.createdAt) > new Date(lastMessage?.createdAt ?? '0');

    if (lastMessage && isNewMessageCreatedLaster) {
      Object.assign(lastMessage, newMessage);
    }

    let closestMessageIndexByCreatedAt = -1;

    // TODO: refactor when ts version >= 5.3.3
    for (let index = messageChat.messages.items.length - 1; index >= 0; index -= 1) {
      // eslint-disable-next-line max-len
      if (new Date(newMessage.createdAt) > new Date(messageChat.messages.items[index].createdAt)) {
        closestMessageIndexByCreatedAt = index + 1;
        break;
      }
    }

    if (closestMessageIndexByCreatedAt === -1) {
      messageChat.messages.items.splice(0, 0, newMessage);
    } else {
      messageChat.messages.items.splice(closestMessageIndexByCreatedAt, 0, newMessage);
    }
  };

  const updateMessage = (updatedMessage: Partial<IMessage>, chatId: string): boolean => {
    const messageChat = state.value.chats.find((chat) => chat.id === chatId);
    let isUpdatedLastMessage = false;
    let isUpdatedMessage = false;

    if (messageChat === undefined) return false;

    if (messageChat.lastMessage) {
      if (messageChat.lastMessage?.id === updatedMessage.id) {
        Object.assign(messageChat.lastMessage, updatedMessage);
        isUpdatedLastMessage = true;
      }
    }

    if (messageChat.messages === null) return false;

    if (messageChat) {
      const existMessage = messageChat.messages.items.find(
        (message) => message.id === updatedMessage.id,
      );

      if (existMessage) {
        Object.assign(existMessage, updatedMessage);
        isUpdatedMessage = true;
      }
    }

    if (isUpdatedLastMessage || isUpdatedMessage) return true;

    return false;
  };

  const setDeleteDateMessage = (messageId: number, chatId: string, deleteTime: string | null) => {
    const messageChat = state.value.chats.find((chat) => chat.id === chatId);

    if (messageChat === undefined) return;
    if (messageChat.messages === null) return;

    if (messageChat.lastMessage?.id === messageId) {
      messageChat.lastMessage.deletedDate = deleteTime;
    }

    const deletedMessage = messageChat.messages.items.find(
      (message) => message.id === messageId,
    );

    if (deletedMessage) {
      deletedMessage.deletedDate = deleteTime;
    }
  };

  return {
    addTemporaryMessage,
    updateMessage,
    confirmMessage,
    onReciveNewMessage,
    setDeleteDateMessage,
    insertMessageByCreatedAt,
  };
};
