import { IMessage, useChatStore, IChatDetails } from '@/shared';

export const useConversationDetailsService = () => {
  const { getChatById } = useChatStore();

  const getChatDetails = (chatId: string): IChatDetails | null => {
    const chat = getChatById(chatId);

    if (chat) {
      return chat.details;
    }

    return null;
  };

  const getChatScrollPosition = (chatId: string): number | null => {
    const chat = getChatById(chatId);

    if (chat) {
      return chat.details.scrollPosition;
    }

    return null;
  };

  const setMessageValue = (chatId: string, message: string) => {
    const chat = getChatById(chatId);

    if (chat) {
      chat.details.messageValue = message;
    }
  };

  const setToolbarValue = (chatId: string, toolbar: boolean) => {
    const chat = getChatById(chatId);

    if (chat) {
      chat.details.toolbar = toolbar;
    }
  };

  const setReplyMessage = (chatId: string, replyMessage: IMessage | null) => {
    const chat = getChatById(chatId);

    if (chat) {
      chat.details.reply = replyMessage;
    }
  };

  const updateScrollStatus = (chatId: string, status: boolean) => {
    const chat = getChatById(chatId);

    if (chat) {
      chat.details.isScrollAtBottom = status;
    }
  };

  const saveScrollPosition = (chatId: string, scrollPosition: number) => {
    const chat = getChatById(chatId);

    if (chat) {
      chat.details.scrollPosition = scrollPosition;
    }
  };

  return {
    setMessageValue,
    setReplyMessage,
    setToolbarValue,
    saveScrollPosition,
    getChatDetails,
    getChatScrollPosition,
    updateScrollStatus,
  };
};
