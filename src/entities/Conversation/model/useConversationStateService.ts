import { computed } from 'vue';
import { storeToRefs } from 'pinia';
import { useChatStore, IChat } from '@/shared';
import { defineLastActionInChats } from '../lib/defineLastActionInChats';

export const useConversationStateService = () => {
  const { state } = storeToRefs(useChatStore());

  const addNewChat = async (newChat: IChat) => {
    const existTemporaryChat = state.value.chats.find(
      (chat) => newChat.getId() === chat.getId(),
    );

    // TODO: fix kludge, that merge members
    if (existTemporaryChat) {
      // eslint-disable-next-line no-param-reassign
      newChat.members = existTemporaryChat.members;
    }

    if (existTemporaryChat) {
      Object.assign(existTemporaryChat, newChat);
    } else {
      state.value.chats.push(newChat);
    }
  };

  const setUserChats = (chats: IChat[]): IChat[] => {
    state.value.chats = chats;

    return state.value.chats;
  };

  const setLastRead = (chatId: string, lastRead: string): void => {
    const foundChat = state.value.chats.find((chat) => chat.id === chatId);

    if (foundChat !== undefined) {
      foundChat.lastRead = lastRead;
    }
  };

  const updateChat = (updatedChat: IChat): void => {
    const chatToUpdateIndex = state.value.chats.findIndex((chat) => chat.id === updatedChat.id);

    state.value.chats.splice(chatToUpdateIndex, 1, updatedChat);
  };

  const deleteMember = (chatId: string, memberId: string): void => {
    const foundChat = state.value.chats.find((chat) => chat.id === chatId);

    if (foundChat) {
      foundChat.members = foundChat.members.filter((member) => member.id !== memberId);
    }
  };

  const deleteChat = (chatId: string) => {
    setUserChats(state.value.chats.filter((chat) => chat.id !== chatId));
  };

  const setNewChatMessages = (newChat: IChat): void => {
    const messageChatIndex = state.value.chats.findIndex(
      (chat) => newChat.id === chat.id,
    );

    const chatMessages = state.value.chats[messageChatIndex].messages;

    if (newChat.messages === null || chatMessages === null) return;

    if (newChat.messages.meta.currentPage > chatMessages.meta.currentPage) {
      chatMessages.items = [...newChat.messages.items, ...chatMessages.items];
      chatMessages.meta = newChat.messages.meta;
    }
  };

  const getChats = computed(() => state.value.chats.sort((a, b) => defineLastActionInChats(a, b)));

  return {
    setUserChats,
    setLastRead,
    addNewChat,
    getChats,
    deleteChat,
    deleteMember,
    updateChat,
    setNewChatMessages,
  };
};
