import { IChat } from '@/shared';

const getClosestActionDate = (userChat: IChat): Date => {
  if (userChat.lastMessage) {
    if (new Date(userChat.updatedAt) > new Date(userChat.lastMessage.updatedAt)) {
      return new Date(userChat.updatedAt);
    }

    return new Date(userChat.lastMessage.updatedAt);
  }

  return new Date(userChat.updatedAt);
};

export const defineLastActionInChats = (chat1: IChat, chat2: IChat) => {
  const chatDate1 = getClosestActionDate(chat1);
  const chatDate2 = getClosestActionDate(chat2);

  if (chatDate1 > chatDate2) {
    return -1;
  }

  if (chatDate1 < chatDate2) {
    return 1;
  }

  return 0;
};
