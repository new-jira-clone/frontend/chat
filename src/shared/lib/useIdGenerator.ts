export default () => {
  const getCurrentTimestamp = () => {
    const timestamp = new Date().valueOf();

    return timestamp;
  };

  return {
    getCurrentTimestamp,
  };
};
