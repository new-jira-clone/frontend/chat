interface LoadingPageOptions {
  totalPages: number;
  topBoundary: number;
  bottomBoundary: number;
  nextPage: number;
  previousPage: number;
}

export default () => {
  const getPageToLoadWithReverseDirection = (event: Event, options: LoadingPageOptions): number => {
    if (!event.target) return 0;

    const { scrollTop, offsetHeight, scrollHeight } = event.target as HTMLElement;

    if (options.nextPage + 1 <= options.totalPages && Math.floor(scrollTop) <= options.topBoundary) {
      return options.nextPage + 1;
    }

    const bottomPosition = Math.floor(scrollHeight - (scrollTop + offsetHeight));

    if (options.previousPage - 1 > 0 && bottomPosition < options.bottomBoundary) {
      return options.previousPage - 1;
    }

    return 0;
  };

  return {
    getPageToLoadWithReverseDirection,
  };
};
