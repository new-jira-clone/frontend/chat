import {
  computed, ref, unref, watch,
} from 'vue';
import { defineStore } from 'pinia';
import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { IChat } from '../interfaces/Chat.interface';

interface IStore {
  chats: IChat[];
  isFecthChats: boolean;
}

export const useChatStore = defineStore('chat', () => {
  const { socket } = useSocketStore();

  const state = ref<IStore>({
    chats: [],
    isFecthChats: false,
  });

  const isFetchChats = computed(() => state.value.isFecthChats);

  const getChatById = (chatId: string) => state.value.chats.find(
    (chat) => chat.id === unref(chatId) || ('interlocutor' in chat && chat.interlocutor.id === unref(chatId)),
  );

  const getChatIndexById = (chatId: string) => state.value.chats.findIndex(
    (chat) => chat.id === unref(chatId) || ('interlocutor' in chat && chat.interlocutor.id === unref(chatId)),
  );

  const initFetchingChatsStatus = () => {
    state.value.isFecthChats = true;
  };

  const $reset = () => {
    state.value.chats = [];
  };

  watch(socket, (newSocket) => {
    if (newSocket === null) {
      $reset();
    }
  });

  return {
    $reset,
    state,
    getChatById,
    getChatIndexById,
    isFetchChats,
    initFetchingChatsStatus,
  };
});
