import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import { IUser, IPageMeta } from '@new-jira-clone/libs/src/types';
import { IUsersList } from '../interfaces/User.interface';

interface IUsersListStore {
  users: IUsersList | null;
  isInitList: boolean;
}

export const useUsersListStore = defineStore('users', () => {
  const state = ref<IUsersListStore>({
    users: null,
    isInitList: false,
  });

  const addNewUsers = (newUsers: IUser[], meta: IPageMeta): IUsersList => {
    if (state.value.users === null) {
      state.value.users = {
        items: newUsers,
        meta,
      };

      return state.value.users;
    }

    state.value.users.items.push(...newUsers);
    state.value.users.meta = meta;

    return state.value.users;
  };

  const listUsers = computed(() => state.value.users?.items ?? []);

  const isInitList = computed(() => state.value.isInitList);

  const setInitStatus = (status: boolean) => {
    state.value.isInitList = status;
  };

  return {
    isInitList,
    listUsers,
    addNewUsers,
    setInitStatus,
  };
});
