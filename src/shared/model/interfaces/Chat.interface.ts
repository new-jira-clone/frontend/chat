import { IPageMeta } from '@new-jira-clone/libs/src/types';
import { ChatTypes } from '../constants';
import { IMessage } from './Message.interface';
import { IUser } from './User.interface';

// TODO: вынести в общий тип пагинации
interface IChatMessages {
  items: Array<IMessage>;
  meta: IPageMeta;
}

export interface IChatDetails {
  messageValue: string;
  toolbar: boolean;
  isScrollAtBottom: boolean;
  scrollPosition: number | null;
  reply: IMessage | null;
}

export interface IBaseChat {
  id: string;
  name: string;
  lastRead: string | null;
  lastMessage: IMessage | null;
  avatar: null | string | string[];
  updatedAt: string;
  createdAt: string;
  members: IUser[];
}

export interface IGroupChat extends IBaseChat {
  type: ChatTypes.Group;
}

export interface IPrivateChat extends IBaseChat {
  type: ChatTypes.Private;
  interlocutor: IUser;
}

export interface ITemporaryChat extends IBaseChat {
  type: ChatTypes.Temporary;
  interlocutor: IUser;
}

export type IChat = (IGroupChat | IPrivateChat | ITemporaryChat) & {
  details: IChatDetails;
} & {
  messages: IChatMessages | null;
  getId: () => string;
};

export interface ICreateGroupChatForm {
  name: string;
  avatar: string;
}
