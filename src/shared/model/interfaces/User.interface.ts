import { IPageMeta } from '@new-jira-clone/libs/src/types';
import { IUserResponse } from '../../api/interfaces';

export type IUser = IUserResponse;

export interface IUsersList {
  items: Array<IUser>;
  meta: IPageMeta;
}
