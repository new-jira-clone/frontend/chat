import { MessageTypes } from '../constants';
import { IUser } from './User.interface';

export interface IMessageDetails {
  editMessageValue?: string;
  isEdit?: boolean;
  isEditToolbar?: boolean;
  isShowReplyInEditMessage?: boolean;
}

export interface IMessage {
  id: number;
  content: string;
  shortContent: string;
  extra?: string;
  updatedAt: string;
  createdAt: string;
  deletedDate: string | null;
  delivered: boolean;
  type: MessageTypes;
  replyTo?: IMessage;
  details: IMessageDetails;
  author: IUser;
}

export interface ISendMessageForm {
  temporaryId: number;
  chatId: string;
  replyMessageId?: number;
  content: string;
  shortContent: string;
}

export interface IEditMessageForm {
  messageId: number;
  chatId: string;
  content: string;
  replyMessageId?: null;
  shortContent: string;
}

export interface IDeleteMessageForm {
  messageId: number;
  chatId: string;
}

export interface IDeleteMessage {
  messageId: number;
  chatId: string;
}

export interface IRestoreMessageForm {
  messageId: number;
  chatId: string;
}

export interface IRestoreMessage {
  message: IMessage;
  chatId: string;
}

export interface INewMessage extends IMessage {
  chat: {
    id: string;
  };
}

export interface MessageLeaveFromGroup {
  message: INewMessage;
  leavedUserId: string;
}

export interface IUpdatedMessage {
  message: Pick<Partial<IMessage>, 'id'>;
  chatId: string;
}

export interface IMessageTyping {
  user: {
    id: string;
  };
  chat: {
    id: string;
  };
}
