export enum ChatTypes {
  Private = 'private',
  Group = 'group',
  Temporary = 'temporary',
}

export enum MessageTypes {
  Default = 'message',
  CreateGroup = 'create-group',
  LeaveGroup = 'leave-group',
  CallOffer = 'call-offer',
  DeclineCallOffer = 'decline-call-offer',
  StartCall = 'start-call',
  EndCall = 'end-call',
}

export const MessageStatus = {
  notDelivered: 'notDelivered',
} as const;

export const pathConfig = {
  chat: '/chat',
} as const;

export type IMessageStatus = (typeof MessageStatus)[keyof typeof MessageStatus];
