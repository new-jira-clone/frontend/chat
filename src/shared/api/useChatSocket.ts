/* eslint-disable arrow-body-style */
import { useSocketStore } from '@new-jira-clone/libs/src/store';
import {
  IUser,
  IEditMessageForm,
  ISendMessageForm,
  IDeleteMessageForm,
  ICreateGroupChatForm,
  IRestoreMessageForm,
} from '@/shared';

export const useChatSocket = () => {
  const ACK_DELAY = 5000;
  const { socket } = useSocketStore();

  const loadMessagesWithAck = (chatId: string, page = 1, delay = ACK_DELAY) => {
    return socket.value?.timeout(delay)?.emitWithAck('loadMessages', { chatId, page });
  };

  const createGroupChat = (users: IUser[], form: ICreateGroupChatForm, delay = ACK_DELAY) => {
    return socket.value?.timeout(delay).emitWithAck('createGroupChat', {
      name: form.name,
      avatar: form.avatar,
      usersIds: users.map((user) => user.id),
    });
  };

  const createPrivateChatWithAck = (
    interlocutorId: string,
    message: Pick<ISendMessageForm, 'content' | 'shortContent'>,
    delay = ACK_DELAY,
  ) => {
    return socket.value?.timeout(delay).emitWithAck('createPrivateChat', {
      interlocutorId,
      content: message.content,
      shortContent: message.shortContent,
    });
  };

  const deleteMessage = (messageForm: IDeleteMessageForm) => {
    socket.value?.emit('deleteMessage', messageForm);
  };

  const sendMessage = (messageForm: ISendMessageForm) => {
    socket.value?.emit('sendMessage', messageForm);
  };

  const restoreMessage = (messageForm: IRestoreMessageForm) => {
    socket.value?.emit('restoreMessage', messageForm);
  };

  const editMessage = (messageForm: IEditMessageForm) => {
    socket.value?.emit('editMessage', messageForm);
  };

  const startTypingMessage = (chatId: string) => {
    socket.value?.emit('startMessageTyping', { chatId });
  };

  const stopTypingMessage = (chatId: string) => {
    socket.value?.emit('stopMessageTyping', { chatId });
  };

  const readMessage = (chatId: string, time: string) => {
    socket.value?.emit('readMessage', { chatId, time });
  };

  const deletePrivateChat = (chatId: string) => {
    socket.value?.emit('deletePrivateChat', { chatId });
  };

  const leaveGroup = (chatId: string) => {
    socket.value?.emit('leaveChat', { chatId });
  };

  const getChats = () => {
    socket.value?.emit('getChats');
  };

  return {
    loadMessagesWithAck,
    socket,
    getChats,
    leaveGroup,
    deletePrivateChat,
    createGroupChat,
    createPrivateChatWithAck,
    startTypingMessage,
    stopTypingMessage,
    restoreMessage,
    sendMessage,
    editMessage,
    deleteMessage,
    readMessage,
  };
};
