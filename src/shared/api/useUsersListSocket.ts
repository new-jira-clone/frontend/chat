import { storeToRefs } from 'pinia';
import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { useUsersListStore } from '../model/store/useUsersListStore';
import usersListSerializer from './serializer/usersList.serializer';

export const useUsersListSocket = () => {
  const getListUsers = async (page = 1, limit = 15): Promise<void> => {
    const usersStore = useUsersListStore();

    const { isInitList } = storeToRefs(usersStore);

    if (isInitList.value) return;

    const { socket } = useSocketStore();
    const { setInitStatus, addNewUsers } = usersStore;

    setInitStatus(true);

    const DELAY_ACK = 5000;

    try {
      const usersListResponse = await socket.value?.timeout(DELAY_ACK).emitWithAck('getUsers', {
        page,
        limit,
      });

      const serializedUsers = usersListSerializer(usersListResponse);

      addNewUsers(serializedUsers.items, serializedUsers.meta);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    getListUsers,
  };
};
