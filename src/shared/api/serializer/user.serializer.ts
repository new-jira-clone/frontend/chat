import { IUser } from '@/shared';
import { IUserResponse } from '../interfaces';

export default (userResponse: IUserResponse): IUser => userResponse;
