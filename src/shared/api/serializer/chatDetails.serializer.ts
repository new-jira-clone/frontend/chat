import { IChat, IMessage } from '@/shared';
import { IChatDetailsResponse } from '../interfaces';
import messageSerailizer from './message.serailizer';
import chatSerializer from './chat.serializer';

export default (chatDetailsResponse: IChatDetailsResponse): IChat => {
  const chat = {
    ...chatSerializer(chatDetailsResponse.chat),
    messages: chatDetailsResponse.messages,
  };

  chat.messages.items = chatDetailsResponse.messages.items
    .map((message) => messageSerailizer(message) as IMessage)
    .reverse();

  return chat as IChat;
};
