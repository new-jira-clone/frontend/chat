import { IMessage } from '@/shared';
import { IMessageResponse } from '../interfaces';
import userSerializer from './user.serializer';

export default (messageResponse: IMessageResponse): IMessage | null => {
  if (!messageResponse) return null;

  return {
    ...messageResponse,
    delivered: true,
    details: {},
    author: userSerializer(messageResponse.author),
  };
};
