import { ChatTypes, IBaseChat, IChat } from '@/shared';
import { IChatResponse } from '../interfaces';
import messageSerailizer from './message.serailizer';
import userSerializer from './user.serializer';

export default (chatResponse: IChatResponse): IChat => {
  const details = {
    messageValue: '',
    toolbar: false,
    reply: null,
    scrollPosition: null,
    isScrollAtBottom: false,
  };

  const result: IBaseChat = {
    id: chatResponse.id,
    name: chatResponse.name,
    lastRead: chatResponse.lastRead,
    lastMessage: messageSerailizer(chatResponse.lastMessage),
    avatar: chatResponse.avatar ? chatResponse.avatar : null,
    updatedAt: chatResponse.updatedAt,
    createdAt: chatResponse.createdAt,
    members: chatResponse.members?.map((member) => userSerializer(member)) ?? [],
  };

  if (chatResponse.type === ChatTypes.Private || chatResponse.type === ChatTypes.Temporary) {
    return Object.assign(result, {
      id: chatResponse.id,
      type: chatResponse.type,
      details,
      messages: null,
      interlocutor: userSerializer(chatResponse.interlocutor),
      getId() {
        return this.interlocutor.id;
      },
    });
  }

  return Object.assign(result, {
    id: chatResponse.id,
    type: chatResponse.type,
    messages: null,
    details,
    getId() {
      return this.id;
    },
  });
};
