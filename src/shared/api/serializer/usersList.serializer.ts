import { IUsersList } from '@/shared';
import { IUsersListResponse } from '../interfaces';
import userSerializer from './user.serializer';

export default (usersListResponse: IUsersListResponse): IUsersList => {
  const usersList: IUsersList = {
    items: usersListResponse.items.map((user) => userSerializer(user)),
    meta: usersListResponse.meta,
  };

  return usersList;
};
