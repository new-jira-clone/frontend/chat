import chatSerializer from './chat.serializer';
import messageSerailizer from './message.serailizer';
import userSerializer from './user.serializer';
import usersListSerializer from './usersList.serializer';

export default {
  chatSerializer,
  messageSerailizer,
  userSerializer,
  usersListSerializer,
};
