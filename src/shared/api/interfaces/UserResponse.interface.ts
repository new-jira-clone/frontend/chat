import { IPageMeta } from '@new-jira-clone/libs/src/types';

export interface IUserResponse {
  id: string;
  name: string;
  email: string;
  avatar: string;
}

export interface IUsersListResponse {
  items: IUserResponse[];
  meta: IPageMeta;
}
