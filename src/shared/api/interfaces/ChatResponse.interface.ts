import { IPageMeta } from '@new-jira-clone/libs/src/types';
import { ChatTypes } from '../../model/constants';
import { IMessageResponse } from './MessageResponse.interface';
import { IUserResponse } from './UserResponse.interface';

interface IBaseChatResponse {
  id: string;
  name: string;
  lastRead: string | null;
  lastMessage: IMessageResponse;
  avatar: null | string | string[];
  updatedAt: string;
  createdAt: string;
  members: IUserResponse[];
}

export interface IPrivateChatResponse extends IBaseChatResponse {
  type: ChatTypes.Private;
  interlocutor: IUserResponse;
}

export interface IGroupChatResponse extends IBaseChatResponse {
  type: ChatTypes.Group;
}

export interface ITemporaryChatResponse extends IBaseChatResponse {
  type: ChatTypes.Temporary;
  interlocutor: IUserResponse;
}

export type IChatResponse = IPrivateChatResponse | IGroupChatResponse | ITemporaryChatResponse;

export interface IChatDetailsResponse {
  chat: IChatResponse;
  messages: {
    items: IMessageResponse[];
    meta: IPageMeta;
  };
}
