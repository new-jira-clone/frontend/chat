import { IUserResponse } from './UserResponse.interface';
import { MessageTypes } from '../../model/constants';

export interface IMessageResponse {
  id: number;
  content: string;
  shortContent: string;
  updatedAt: string;
  deletedDate: string | null;
  createdAt: string;
  type: MessageTypes;
  author: IUserResponse;
}
