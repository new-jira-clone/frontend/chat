import { ref } from 'vue';
import { IChat, useChatStore } from '@/shared';
import { IChatDetailsResponse, useChatSocket } from '@/shared/api';
import chatDetailsSerializer from '@/shared/api/serializer/chatDetails.serializer';

export default () => {
  const isLoadedChat = ref(false);

  const fetchChatDetails = async (chatId: string, page: number): Promise<IChat | null> => {
    const { loadMessagesWithAck } = useChatSocket();

    try {
      const chatDetailsResponse: IChatDetailsResponse = await loadMessagesWithAck(String(chatId), page);
      const serializedChatDetails = chatDetailsSerializer(chatDetailsResponse);

      return serializedChatDetails;
    } catch (error) {
      console.log(error);
    }

    return null;
  };

  const fetchChatDetailsIfNotSaved = async (chatId: string): Promise<IChat | null> => {
    const { getChatById: getSavedChatById } = useChatStore();

    const foundChat = getSavedChatById(chatId);

    if (!foundChat?.messages) {
      isLoadedChat.value = false;

      const chatDetails = await fetchChatDetails(chatId, 1);

      isLoadedChat.value = true;

      return chatDetails;
    }

    isLoadedChat.value = true;

    return null;
  };

  return {
    isLoadedChat,
    fetchChatDetails,
    fetchChatDetailsIfNotSaved,
  };
};
