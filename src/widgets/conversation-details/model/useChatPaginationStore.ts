import { ref } from 'vue';
import { defineStore } from 'pinia';

export interface IChatPagination {
  loadedPages: Set<number>;
}

interface IStore {
  [chatId: string]: IChatPagination;
}

export const useChatPaginationStore = defineStore('chatPagination', () => {
  const state = ref<IStore>({});

  const saveNewLoadedPage = (chatId: string, loadedPage: number): void => {
    state.value[chatId]?.loadedPages.add(loadedPage);
  };

  const isLoadedPage = (chatId: string, page: number): boolean => !!state.value[chatId]?.loadedPages.has(page);

  const setDefaultPaginationStructure = (chatId: string): void => {
    state.value[chatId] = {
      loadedPages: new Set([1]),
    };
  };

  return {
    state,
    saveNewLoadedPage,
    isLoadedPage,
    setDefaultPaginationStructure,
  };
});
