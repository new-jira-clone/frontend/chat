import {
  ref, onUnmounted, Ref, watch,
} from 'vue';
import { useStickyScroll } from '@new-jira-clone/libs/src/composables/scroll/useStickyScroll';

export default (
  viewportRef: Ref<HTMLElement | null>,
  scrollContentRef: Ref<HTMLElement | null>,
  isScrollAtBottom: Ref<boolean>,
) => {
  let scrollContentObserver: ResizeObserver | null = null;
  let viewportObserver: ResizeObserver | null = null;

  const currentScrollContentHeight = ref(0);
  let currentViewportHeight = 0;
  let previousVieportHeight = 0;

  const moveScrollPositionToOffsetOnChangeViewportHeight = () => {
    if (previousVieportHeight === 0 || currentViewportHeight === 0) return;
    if (currentViewportHeight === previousVieportHeight) return;
    if (!viewportRef.value) return;

    const { moveScrollTo } = useStickyScroll();

    const isNewViewportHeightIsBiggerThanPrevious = currentViewportHeight > previousVieportHeight;

    if (isScrollAtBottom.value && isNewViewportHeightIsBiggerThanPrevious) return;

    if (isNewViewportHeightIsBiggerThanPrevious) {
      moveScrollTo(
        viewportRef.value,
        viewportRef.value.scrollTop - (currentViewportHeight - previousVieportHeight),
      );
    } else {
      moveScrollTo(
        viewportRef.value,
        viewportRef.value.scrollTop + (previousVieportHeight - currentViewportHeight),
      );
    }
  };

  watch(scrollContentRef, () => {
    if (scrollContentRef.value === null) return;

    scrollContentObserver?.disconnect();

    const createdScrollObserver = new ResizeObserver((entries: ResizeObserverEntry[]) => {
      // eslint-disable-next-line no-restricted-syntax
      for (const entry of entries) {
        const { height } = entry.contentRect;

        if (!height) return;

        currentScrollContentHeight.value = height;
      }
    });

    createdScrollObserver.observe(scrollContentRef.value);

    scrollContentObserver = createdScrollObserver;
  });

  watch(viewportRef, () => {
    if (!viewportRef.value) return;

    viewportObserver?.disconnect();

    const createdScrollObserver = new ResizeObserver((entries: ResizeObserverEntry[]) => {
      // eslint-disable-next-line no-restricted-syntax
      for (const entry of entries) {
        const { height } = entry.contentRect;

        if (!height) return;

        previousVieportHeight = currentViewportHeight;
        currentViewportHeight = height;

        moveScrollPositionToOffsetOnChangeViewportHeight();
      }
    });

    createdScrollObserver.observe(viewportRef.value);

    viewportObserver = createdScrollObserver;
  });

  onUnmounted(() => {
    scrollContentObserver?.disconnect();
    viewportObserver?.disconnect();
  });

  return {
    currentScrollContentHeight,
  };
};
