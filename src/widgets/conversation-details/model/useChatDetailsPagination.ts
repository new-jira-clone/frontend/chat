import {
  computed, ref, Ref, watch,
} from 'vue';
import { useConversationStateService } from '@/entities/Conversation';
import { IChat, useCheckScrollPosition } from '@/shared';
import useFetchChatDetails from './useFetchChatDetails';
import { useChatPaginationStore } from './useChatPaginationStore';

export default (chat: Ref<IChat>) => {
  const chatPagination = useChatPaginationStore();
  const currentShowingPages = ref<{ [chatId: string]: number[] }>({});

  const { isLoadedPage, saveNewLoadedPage, setDefaultPaginationStructure } = chatPagination;
  const { fetchChatDetails } = useFetchChatDetails();
  const { getPageToLoadWithReverseDirection } = useCheckScrollPosition();
  const { setNewChatMessages } = useConversationStateService();

  const addNewPageAtStart = (chatId: string, page: number): void => {
    if (currentShowingPages.value[chatId] === undefined) return;

    currentShowingPages.value[chatId].unshift(page);
  };

  const addNewPageAtEnd = (chatId: string, page: number): void => {
    if (currentShowingPages.value[chatId] === undefined) return;

    currentShowingPages.value[chatId].push(page);
  };

  const removeLastPage = (chatId: string): void => {
    if (currentShowingPages.value[chatId] === undefined) return;

    currentShowingPages.value[chatId].pop();
  };

  const removeFirstPage = (chatId: string): void => {
    if (currentShowingPages.value[chatId] === undefined) return;

    currentShowingPages.value[chatId].shift();
  };

  const getFirstPage = (chatId: string): number | null => {
    if (currentShowingPages.value[chatId] === undefined) return null;

    return currentShowingPages.value[chatId][0];
  };

  const getLastPage = (chatId: string): number | null => {
    if (currentShowingPages.value[chatId] === undefined) return null;

    return currentShowingPages.value[chatId][currentShowingPages.value[chatId].length - 1];
  };

  const getPagesLength = (chatId: string): number => {
    if (currentShowingPages.value[chatId] === undefined) return 0;

    return currentShowingPages.value[chatId].length;
  };

  const messagesInCurrentPages = computed(() => {
    if (!chat.value?.messages) return [];

    const totalItems = chat.value.messages.items.length;
    const itemsPerPage = chat.value.messages.meta.itemsPerPage;
    const chatId = chat.value.getId();

    const messagesInPages = currentShowingPages.value[chatId].map((page) => {
      const messagesInPage = chat.value.messages?.items.slice(
        Math.max(0, totalItems - page * itemsPerPage),
        totalItems - (page - 1) * itemsPerPage,
      );

      return messagesInPage ?? [];
    });

    return messagesInPages.flat(1);
  });

  const fetchNewChatMetaIfNotSaved = async (page: number): Promise<IChat | null> => {
    if (isLoadedPage(chat.value.getId(), page)) return null;

    saveNewLoadedPage(chat.value.getId(), page);

    return fetchChatDetails(chat.value.getId(), page);
  };

  const initDefaultPagination = () => {
    if (chat.value) {
      setDefaultPaginationStructure(chat.value.getId());
    }
  };

  const limitScrollPosition = (event: Event, scrollContentHeight: number) => {
    const target = event.target as HTMLElement;

    if (target.scrollTop === 0) {
      target.scrollTop = 1;
      return;
    }

    if (target.scrollTop === scrollContentHeight) {
      target.scrollTop = scrollContentHeight - 1;
    }
  };

  const getNewMessagesByScrollPosition = async (event: Event, scrollContentHeight: number): Promise<number | null> => {
    if (!chat.value?.messages) return null;

    const firstPage = getFirstPage(chat.value.getId()) ?? 1;
    const lastPage = getLastPage(chat.value.getId()) ?? 1;

    const newPage = getPageToLoadWithReverseDirection(event, {
      nextPage: firstPage,
      previousPage: lastPage,
      topBoundary: 150,
      bottomBoundary: 150,
      totalPages: chat.value.messages.meta.totalPages,
    });

    if (newPage === 0) return null;

    limitScrollPosition(event, scrollContentHeight);

    const newChatMeta = await fetchNewChatMetaIfNotSaved(newPage);

    if (newChatMeta) {
      setNewChatMessages(newChatMeta);
    }

    const MAX_SHOWING_PAGES = 4;

    if (newPage > firstPage) {
      addNewPageAtStart(chat.value.getId(), newPage);

      if (getPagesLength(chat.value.getId()) > MAX_SHOWING_PAGES) {
        removeLastPage(chat.value.getId());
      }

      return newPage;
    }

    addNewPageAtEnd(chat.value.getId(), newPage);

    if (getPagesLength(chat.value.getId()) > MAX_SHOWING_PAGES) {
      removeFirstPage(chat.value.getId());
    }

    return newPage;
  };

  watch(
    chat,
    () => {
      if (!chat.value) return;

      const DEFAULT_PAGE_LOADED = 1;

      currentShowingPages.value[chat.value.getId()] = [DEFAULT_PAGE_LOADED];
    },
    { immediate: true },
  );

  return {
    initDefaultPagination,
    messagesInCurrentPages,
    getNewMessagesByScrollPosition,
  };
};
