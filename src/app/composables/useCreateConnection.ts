import { useRoute, useRouter } from 'vue-router';
import { useTypingMessages } from '@/features/typing-message';
import { IDeletedRoom, useConversationStateService } from '@/entities/Conversation';
import { useMessageStateService } from '@/entities/Message';
import { useChatSocket, IChatResponse } from '@/shared/api';
import {
  INewMessage, IMessageTyping, IUpdatedMessage,
  IRestoreMessage,
  IDeleteMessage,
  useChatStore,
  MessageLeaveFromGroup,
} from '@/shared';
import chatSerializer from '@/shared/api/serializer/chat.serializer';

export const useCreateConnection = () => {
  const router = useRouter();
  const route = useRoute();

  const { getChats, socket } = useChatSocket();

  const { initFetchingChatsStatus } = useChatStore();

  const {
    addNewChat,
    updateChat,
    deleteChat,
    deleteMember,
    setUserChats,
  } = useConversationStateService();

  const {
    confirmMessage,
    updateMessage,
    setDeleteDateMessage,
    onReciveNewMessage,
    insertMessageByCreatedAt,
  } = useMessageStateService();

  const { removeTypingMessage, addTypingMessage } = useTypingMessages();

  const createChatConnect = () => {
    getChats();

    socket.value?.on('chats', (event: IChatResponse[]): void => {
      initFetchingChatsStatus();
      setUserChats(event.map((chat) => chatSerializer(chat)));
    });

    socket.value?.on('newChat', (event: IChatResponse): void => {
      addNewChat(chatSerializer(event));
    });

    socket.value?.on('updatedChat', (event: IChatResponse): void => {
      updateChat(chatSerializer(event));
    });

    socket.value?.on('newMessage', (event: INewMessage): void => {
      onReciveNewMessage(event);
      removeTypingMessage({
        chat: {
          id: event.chat.id,
        },
        user: {
          id: event.author.id,
        },
      });
    });

    socket.value?.on('messageLeaveGroup', (event: MessageLeaveFromGroup): void => {
      onReciveNewMessage(event.message);
      deleteMember(event.message.chat.id, event.leavedUserId);
    });

    socket.value?.on('deleteMessage', (event: IDeleteMessage) => {
      setDeleteDateMessage(event.messageId, event.chatId, new Date().toISOString());
    });

    socket.value?.on('restoreMessage', (event: IRestoreMessage) => {
      const isUpdateMessage = updateMessage(event.message, event.chatId);

      if (isUpdateMessage === false) {
        insertMessageByCreatedAt(event.message, event.chatId);
      }
    });

    socket.value?.on('updatedMessage', async (event: IUpdatedMessage): Promise<void> => {
      updateMessage(event.message, event.chatId);
    });

    socket.value?.on('confirmedMessage', (event: INewMessage, { temporaryId }): void => {
      confirmMessage(Object.assign(event, { delivered: true }), temporaryId);
    });

    socket.value?.on('startMessageTyping', (event: IMessageTyping): void => {
      addTypingMessage(event);
    });

    socket.value?.on('stopMessageTyping', (event: IMessageTyping): void => {
      removeTypingMessage(event);
    });

    socket.value?.on('deletePrivateChat', (event: IDeletedRoom): void => {
      deleteChat(event.chat.id);

      if (route.params.id === event.chat.id) {
        router.push({
          name: 'chat',
        });
      }
    });
  };

  return {
    createChatConnect,
  };
};
