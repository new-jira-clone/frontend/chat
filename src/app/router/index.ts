import { ComponentPublicInstance } from 'vue';
import { pathConfig } from '@/shared';

export default () => {
  const getRoutes = (layout: ComponentPublicInstance) => [
    {
      path: pathConfig.chat,
      name: 'chat',
      component: () => import('../ChatApp.vue'),
      children: [
        {
          path: '',
          component: () => import('../../pages/conversationHome.vue'),
        },
        {
          name: 'chat-details',
          path: ':id',
          component: () => import('../../pages/conversation.vue'),
        },
      ],
      meta: {
        layout,
      },
    },
  ];

  const getPathConfig = pathConfig;

  return {
    getRoutes,
    getPathConfig,
  };
};
