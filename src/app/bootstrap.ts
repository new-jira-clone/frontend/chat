import { createApp } from 'vue';
import ChatApp from './ChatApp.vue';

createApp(ChatApp).mount('#app');
