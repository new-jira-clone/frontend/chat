import UserProfile from './UserProfile.vue';
import UserProfileMenu from './UserProfileMenu.vue';

export { UserProfile, UserProfileMenu };
