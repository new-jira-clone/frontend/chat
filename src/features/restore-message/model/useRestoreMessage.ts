import { useMessageStateService } from '@/entities/Message';
import { useChatSocket } from '@/shared/api';

export default () => {
  const restoreMessage = (messageId: number, chatId: string) => {
    const { restoreMessage: sendRestoreMessage } = useChatSocket();
    const { setDeleteDateMessage } = useMessageStateService();

    setDeleteDateMessage(messageId, chatId, null);
    sendRestoreMessage({
      messageId,
      chatId,
    });
  };

  return {
    restoreMessage,
  };
};
