import {
  type Ref,
  onMounted,
  watch,
  onUnmounted,
} from 'vue';
import useDebounce from '@new-jira-clone/ui/src/composables/utils/useDebounce';
import { useConversationStateService } from '@/entities/Conversation';
import { IMessage } from '@/shared/model';
import { useChatSocket } from '@/shared/api';

export default (viewportRef: Ref<HTMLDivElement | null>, chatId: Ref<string>) => {
  const observedMessages = new Set();

  let observer: IntersectionObserver | null = null;

  const { readMessage: sendReadMessage } = useChatSocket();
  const { setLastRead } = useConversationStateService();

  const setCreatedAtToElementAttribute = (
    messageComponent: HTMLElement,
    message: IMessage,
  ) => {
    messageComponent.setAttribute('data-created-at', message.createdAt);
  };

  const debounceSendReadMessage = useDebounce(sendReadMessage, 500);

  const intersectionCallback = (
    entries: IntersectionObserverEntry[],
    localObserver: IntersectionObserver,
  ) => {
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting) {
        const createdAtMessage = entry.target.getAttribute('data-created-at');

        if (createdAtMessage) {
          debounceSendReadMessage(chatId.value, createdAtMessage);
          setLastRead(chatId.value, createdAtMessage);
        }

        localObserver.disconnect();
      }
    });
  };

  const observeForUnreadMessages = (
    messageElement: HTMLElement,
    message: IMessage,
    lastRead: string | null,
  ): void => {
    if (observedMessages.has(message.id)) return;

    if (
      messageElement
      && observer !== null
      && viewportRef.value !== null
      && (lastRead === null || new Date(message.createdAt) > new Date(lastRead))
    ) {
      setCreatedAtToElementAttribute(messageElement, message);
      observer?.observe(messageElement);
    }
  };

  watch(chatId, () => {
    observer?.disconnect();
    observedMessages.clear();
  });

  onMounted(() => {
    observer = new IntersectionObserver(intersectionCallback, {
      root: viewportRef.value,
      rootMargin: '0px',
      threshold: 0.9,
    });
  });

  onUnmounted(() => {
    observer?.disconnect();
  });

  return { observeForUnreadMessages };
};
