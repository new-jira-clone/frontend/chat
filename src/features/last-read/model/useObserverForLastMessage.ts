import {
  type Ref, onMounted, watch, onUnmounted,
} from 'vue';
import { useConversationDetailsService } from '@/entities/Conversation';
import { IMessage } from '@/shared/model';

export default (viewportRef: Ref<HTMLDivElement | null>, chatId: Ref<string>) => {
  let lastMessageId: number | null = null;
  let observer: IntersectionObserver | null = null;

  const { updateScrollStatus } = useConversationDetailsService();

  const intersectionCallback = (entries: IntersectionObserverEntry[]) => {
    entries.forEach((entry: IntersectionObserverEntry) => {
      if (entry.isIntersecting && entry.target.hasAttribute('data-last-message')) {
        updateScrollStatus(chatId.value, true);
      } else {
        updateScrollStatus(chatId.value, false);
      }
    });
  };

  const observeForLastMessage = (messageElement: HTMLElement, message: IMessage, isLastMessage = false): void => {
    if (isLastMessage && messageElement && observer !== null && viewportRef.value !== null) {
      if (lastMessageId) {
        observer.disconnect();
      }

      lastMessageId = message.id;
      messageElement.setAttribute('data-last-message', '');
      observer?.observe(messageElement);
    }
  };

  watch(chatId, () => {
    lastMessageId = null;
    observer?.disconnect();
  });

  onMounted(() => {
    observer = new IntersectionObserver(intersectionCallback, {
      root: viewportRef.value,
      rootMargin: '0px',
      threshold: 0.99,
    });
  });

  onUnmounted(() => {
    observer?.disconnect();
  });

  return { observeForLastMessage };
};
