import { useConversationDetailsService } from '@/entities/Conversation';
import { useMessageStateService } from '@/entities/Message';
import { IDeleteMessageForm } from '@/shared';
import { useChatSocket } from '@/shared/api';

export default () => {
  const deleteMessage = (messageForm: IDeleteMessageForm) => {
    const { deleteMessage: sendDeleteMessage } = useChatSocket();
    const { setDeleteDateMessage } = useMessageStateService();
    const { setReplyMessage } = useConversationDetailsService();

    setReplyMessage(messageForm.chatId, null);
    sendDeleteMessage(messageForm);
    setDeleteDateMessage(messageForm.messageId, messageForm.chatId, new Date().toISOString());
  };

  return {
    deleteMessage,
  };
};
