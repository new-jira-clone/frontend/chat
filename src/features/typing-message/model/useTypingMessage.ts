import { computed, ref } from 'vue';
import { defineStore } from 'pinia';
import { ICleanUpMessageTypingFunction } from '@/entities/Message';
import { IMessageTyping } from '@/shared';

export const useTypingMessages = defineStore('typingMessages', () => {
  const typingMessages = ref<Array<IMessageTyping & ICleanUpMessageTypingFunction>>([]);

  const removeTypingMessage = (stoppedTypingMessage: IMessageTyping): void => {
    typingMessages.value = typingMessages.value.filter((typingMessage) => {
      const isEqualChat = typingMessage.chat.id === stoppedTypingMessage.chat.id;
      const isEqualUser = typingMessage.user.id === stoppedTypingMessage.user.id;

      if (isEqualChat && isEqualUser) {
        clearTimeout(typingMessage.clearFunction);

        return false;
      }

      return true;
    });
  };

  const addTypingMessage = async (newTypingMessage: IMessageTyping) => {
    removeTypingMessage(newTypingMessage);

    const clearFunction = setTimeout(() => {
      removeTypingMessage(newTypingMessage);
    }, 15000);

    typingMessages.value.push({
      ...newTypingMessage,
      clearFunction,
    });
  };

  const getTypingMessages = computed(() => typingMessages.value);

  return {
    getTypingMessages,
    removeTypingMessage,
    addTypingMessage,
  };
});
