export enum ChatCreateTypes {
  Group = 1,
  Private = 2,
  GroupFinalStep = 3,
}
