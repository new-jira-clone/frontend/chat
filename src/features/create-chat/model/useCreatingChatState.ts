import { ref } from 'vue';
import { IUser } from '@/shared';
import { ChatCreateTypes } from './createChat.enum';

const chatCreateType = ref<ChatCreateTypes | 0>(0);
const selectedUsers = ref<IUser[]>([]);

export default () => {
  const setChatCreatingType = (type: ChatCreateTypes | 0): void => {
    chatCreateType.value = type;
  };

  const showCretingGroupChat = (): void => {
    setChatCreatingType(ChatCreateTypes.Group);
  };

  const showCreatingGroupChatFinalStep = (): void => {
    setChatCreatingType(ChatCreateTypes.GroupFinalStep);
  };

  const showCreatingPrivateChat = (): void => {
    setChatCreatingType(ChatCreateTypes.Private);
  };

  const setSelectedUsers = (newUsers: IUser[]): void => {
    selectedUsers.value = newUsers;
  };

  const $reset = () => {
    setChatCreatingType(0);
    setSelectedUsers([]);
  };

  return {
    $reset,
    chatCreateType,
    showCretingGroupChat,
    showCreatingPrivateChat,
    showCreatingGroupChatFinalStep,
    setChatCreatingType,
    selectedUsers,
    setSelectedUsers,
  };
};
