import { IUser } from '@/shared';

export interface ICreateGroupChatForm {
  users: IUser;
  name: string;
}
