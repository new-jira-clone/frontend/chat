import { computed, inject, Ref } from 'vue';
import { i18n } from '@new-jira-clone/libs/src/types';
import { IMessage } from '@/shared';

interface GroupedMessages {
  time: string;
  messages: IMessage[];
}

export default (messages: Ref<IMessage[]>) => {
  const $t = inject('$t') as i18n;

  const groupedMessages = computed((): GroupedMessages[] => {
    const groupedByTime: { [key: string]: GroupedMessages } = {};
    const options: Intl.DateTimeFormatOptions = { day: 'numeric', month: 'long' };
    const dateNow = new Date().toLocaleDateString();

    messages.value?.forEach((item) => {
      const time = new Date(item.createdAt);
      const timeString = time.toLocaleDateString();
      let groupedTime;

      if (dateNow === timeString) {
        groupedTime = $t('today') as string;
      } else {
        groupedTime = new Intl.DateTimeFormat('en-US', options).format(time);
      }

      if (groupedByTime[timeString]) {
        groupedByTime[timeString].time = groupedTime;
        groupedByTime[timeString].messages.push(item);
      } else {
        groupedByTime[timeString] = {
          time: groupedTime,
          messages: [item],
        };
      }
    });

    const keys = Object.keys(groupedByTime);

    keys.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());

    return keys.map((key) => groupedByTime[key]);
  });

  return {
    groupedMessages,
  };
};
