import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { usePeerConnection } from './usePeerConnection';
import { useMediaStreamStore } from './useMediaStreamStore';
import { type MediaConstraints } from './types';

export default () => {
  const sendAnswer = async (
    chatId: string,
    offer: RTCSessionDescriptionInit,
    constraints: MediaConstraints,
  ): Promise<RTCSessionDescriptionInit> => {
    const { setPeerConnection, watchForPeerConnectionState } = useMediaStreamStore();
    const { socket } = useSocketStore();
    const { createPeerConnection } = usePeerConnection();

    const createdPeerConnection = await createPeerConnection(chatId);

    setPeerConnection(chatId, createdPeerConnection);
    watchForPeerConnectionState(chatId, createdPeerConnection);

    await createdPeerConnection.setRemoteDescription(offer);
    const createdAnswer = await createdPeerConnection.createAnswer();
    await createdPeerConnection.setLocalDescription(createdAnswer);

    createdPeerConnection.addEventListener('negotiationneeded', async () => {
      const { getLocalOptions } = useMediaStreamStore();

      const newCreatedOffer = await createdPeerConnection.createOffer({
        offerToReceiveVideo: true,
      });

      await createdPeerConnection.setLocalDescription(newCreatedOffer);

      socket.value?.emit('description', {
        chatId,
        description: newCreatedOffer,
        video: getLocalOptions(chatId).value.video,
        audio: getLocalOptions(chatId).value.audio,
      });
    });

    socket.value?.emit('answer', {
      chatId,
      answer: createdAnswer,
      audio: constraints.audio,
      video: constraints.video,
    });

    return createdAnswer;
  };

  return {
    sendAnswer,
  };
};
