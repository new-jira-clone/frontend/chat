export const ConnectionStateStatus = {
  ringing: 'ringing',
  connecting: 'connecting',
  connected: 'connected',
} as const;
