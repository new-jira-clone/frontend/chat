import { inject } from 'vue';
import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { DialogPlugin } from '@new-jira-clone/ui/src/plugins/dialog';
import chatSerializer from '@/shared/api/serializer/chat.serializer';
import { IChatResponse } from '@/shared/api';
import { useMediaStreamStore } from './useMediaStreamStore';
import ConnectionOffer from '../ui/ConnectionOffer.vue';
import { usePeerConnection } from './usePeerConnection';

interface ConnectionOfferMessage {
  chat: IChatResponse;
  offer: RTCSessionDescriptionInit;
  video: boolean;
  audio: boolean;
}

interface ConnectionAnswerMessage {
  chatId: string;
  answer: RTCSessionDescriptionInit;
  video: boolean;
  audio: boolean;
}

interface ToggleVideoMessage {
  chatId: string;
  video: boolean;
}

interface ToggleAudioMessage {
  chatId: string;
  audio: boolean;
}

interface StopConnectionMessage {
  chatId: string;
}

interface DescriptionMessage {
  chatId: string;
  description: RTCSessionDescriptionInit;
  video: boolean;
  audio: boolean;
}

export default () => {
  const $dialog = inject('$dialog') as DialogPlugin;
  const { socket } = useSocketStore();
  const {
    setRemoteVideo,
    setRemoteAudio,
    getConnectionByChatId,
    initStreamOptionsStructure,
  } = useMediaStreamStore();
  const { cancelConnection } = usePeerConnection();

  const initListenerCall = () => {
    socket.value?.on('connectionOffer', (message: ConnectionOfferMessage) => {
      const chat = chatSerializer(message.chat);
      const existConnection = getConnectionByChatId(chat.getId());

      if (!existConnection) {
        initStreamOptionsStructure(chat.getId(), { rtcDescription: message.offer });
      }

      setRemoteAudio(chat.getId(), message.audio);
      setRemoteVideo(chat.getId(), message.video);

      $dialog.show(
        ConnectionOffer,
        { contentProps: { connectionTarget: chat } },
      );
    });

    socket.value?.on('connectionAnswer', async (message: ConnectionAnswerMessage) => {
      const existConnection = getConnectionByChatId(message.chatId);

      if (!existConnection) {
        initStreamOptionsStructure(message.chatId);
      }

      if (!existConnection.peerConnection) {
        console.warn('Initial stream options not exist');

        return;
      }

      setRemoteAudio(message.chatId, message.audio);
      setRemoteVideo(message.chatId, message.video);

      const remoteDesc = new RTCSessionDescription(message.answer);

      existConnection.peerConnection.setRemoteDescription(remoteDesc);
    });

    socket.value?.on('candidate', async ({ candidate, chatId }) => {
      try {
        const existConnection = getConnectionByChatId(chatId);

        if (!existConnection.peerConnection) return;

        await existConnection.peerConnection.addIceCandidate(candidate);
      } catch (e) {
        console.error('Error adding received ice candidate', e);
      }
    });

    socket.value?.on('stopCall', (message: StopConnectionMessage) => {
      cancelConnection(message.chatId);
    });

    socket.value?.on('toggleVideo', (message: ToggleVideoMessage) => {
      setRemoteVideo(message.chatId, message.video);
    });

    socket.value?.on('toggleAudio', (message: ToggleAudioMessage) => {
      setRemoteAudio(message.chatId, message.audio);
    });

    socket.value?.on('description', async (message: DescriptionMessage) => {
      const existConnection = getConnectionByChatId(message.chatId);

      if (!existConnection.peerConnection) return;

      const { getLocalOptions } = useMediaStreamStore();

      await existConnection.peerConnection.setRemoteDescription(message.description);

      if (message.description.type === 'offer') {
        await existConnection.peerConnection.setLocalDescription();

        socket.value?.emit('description', {
          chatId: message.chatId,
          description: existConnection.peerConnection.localDescription,
          audio: getLocalOptions(message.chatId).value.audio,
          video: getLocalOptions(message.chatId).value.video,
        });

        setRemoteAudio(message.chatId, message.audio);
        setRemoteVideo(message.chatId, message.video);
      }
    });
  };

  initListenerCall();

  return {};
};
