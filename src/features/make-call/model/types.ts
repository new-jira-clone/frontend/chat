export interface MediaConstraints {
  video: boolean;
  audio: boolean;
}
