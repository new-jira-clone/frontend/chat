import { inject } from 'vue';
import { DialogPlugin } from '@new-jira-clone/ui/src/plugins/dialog';
import { IChat, IUser } from '@/shared';
import { useMediaStreamStore } from './useMediaStreamStore';
import useSendOffer from './useSendOffer';
import ConnectionAnswer from '../ui/ConnectionAnswer.vue';
import { type MediaConstraints } from './types';

export default () => {
  const VIDEO_SETTINGS = {
    width: 1280,
    height: 720,
  };

  const $dialog = inject('$dialog') as DialogPlugin;

  const makeCall = async (chatId: string, connectionTarget: IChat | IUser, constraints: MediaConstraints) => {
    const {
      setLocalStream,
      setLocalAudio,
      setLocalVideo,
      initStreamOptionsStructure,
    } = useMediaStreamStore();
    const { sendOffer } = useSendOffer();

    try {
      const createLocalStream = await navigator.mediaDevices.getUserMedia({
        audio: true,
        video: constraints.video ? VIDEO_SETTINGS : false,
      });

      initStreamOptionsStructure(chatId);
      setLocalAudio(chatId, true);
      setLocalVideo(chatId, constraints.video);
      setLocalStream(chatId, createLocalStream);

      await sendOffer(chatId, constraints);

      $dialog.show(ConnectionAnswer, {
        contentProps: {
          connectionTarget,
        },
      });
    } catch (error) {
      initStreamOptionsStructure(chatId);
      setLocalAudio(chatId, false);
      setLocalVideo(chatId, false);

      await sendOffer(chatId, {
        video: false,
        audio: false,
      });

      $dialog.show(ConnectionAnswer, {
        contentProps: {
          connectionTarget,
        },
      });
    }
  };

  return { makeCall };
};
