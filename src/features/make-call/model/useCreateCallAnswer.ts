import { useMediaStreamStore } from './useMediaStreamStore';
import useSendAnswer from './useSendAnswer';

export default () => {
  const createCallAnswer = async (
    chatId: string,
    offer: RTCSessionDescriptionInit,
    video: boolean,
  ) => {
    const { setLocalStream, setLocalAudio, setLocalVideo } = useMediaStreamStore();
    const { sendAnswer } = useSendAnswer();
    const VIDEO_SETTINGS = {
      width: 1280,
      height: 720,
    };

    try {
      const createdLocalStream = await navigator.mediaDevices.getUserMedia({
        audio: true,
        video: video ? VIDEO_SETTINGS : false,
      });

      setLocalAudio(chatId, true);
      setLocalVideo(chatId, video);
      setLocalStream(chatId, createdLocalStream);
      sendAnswer(chatId, offer, {
        audio: true,
        video,
      });
    } catch (error) {
      setLocalAudio(chatId, false);
      setLocalVideo(chatId, false);
      sendAnswer(chatId, offer, {
        audio: false,
        video: false,
      });
    }
  };

  return {
    createCallAnswer,
  };
};
