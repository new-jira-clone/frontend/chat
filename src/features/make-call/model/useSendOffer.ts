import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { usePeerConnection } from './usePeerConnection';
import { useMediaStreamStore } from './useMediaStreamStore';
import { type MediaConstraints } from './types';

export default () => {
  const sendOffer = async (chatId: string, constraints: MediaConstraints): Promise<RTCSessionDescriptionInit> => {
    const { setPeerConnection, watchForPeerConnectionState } = useMediaStreamStore();
    const { socket } = useSocketStore();
    const { createPeerConnection } = usePeerConnection();

    const createdPeerConnection = await createPeerConnection(chatId);

    setPeerConnection(chatId, createdPeerConnection);
    watchForPeerConnectionState(chatId, createdPeerConnection);

    const createdOffer = await createdPeerConnection.createOffer();
    await createdPeerConnection.setLocalDescription(createdOffer);

    createdPeerConnection.addEventListener('negotiationneeded', async () => {
      const { getLocalOptions } = useMediaStreamStore();

      const newCreatedOffer = await createdPeerConnection.createOffer({
        offerToReceiveVideo: true,
      });

      await createdPeerConnection.setLocalDescription(newCreatedOffer);

      socket.value?.emit('description', {
        chatId,
        description: newCreatedOffer,
        video: getLocalOptions(chatId).value.video,
        audio: getLocalOptions(chatId).value.audio,
      });
    });

    socket.value?.emit('offer', {
      chatId,
      offer: createdOffer,
      audio: constraints.audio,
      video: constraints.video,
    });

    return createdOffer;
  };

  return {
    sendOffer,
  };
};
