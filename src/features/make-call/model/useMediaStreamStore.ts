import { computed, ref } from 'vue';
import { defineStore } from 'pinia';

export type PeerConnectionState = RTCPeerConnectionState | 'ringing';

interface IStreamOptions {
  localOptions: {
    video: boolean;
    audio: boolean;
  };
  remoteOptions: {
    video: boolean;
    audio: boolean;
  };
  localStream: MediaStream | null;
  remoteStream: MediaStream | null;
  peerConnection: RTCPeerConnection | null;
  connectionState: PeerConnectionState;
  rtcDescription: RTCSessionDescriptionInit | null;
}

interface IStream {
  [id: string]: IStreamOptions;
}

export const useMediaStreamStore = defineStore('mediaStreamStore', () => {
  // TODO: optimize ref to shallowRef, customRef etc
  const mediaStreams = ref<IStream>({});

  const initStreamOptionsStructure = (id: string, options: Partial<IStreamOptions> = {}) => {
    mediaStreams.value[id] = {
      localOptions: {
        video: false,
        audio: false,
      },
      remoteOptions: {
        video: false,
        audio: false,
      },
      localStream: null,
      remoteStream: null,
      peerConnection: null,
      connectionState: 'ringing',
      rtcDescription: options.rtcDescription ?? null,
    };
  };

  const setLocalStream = (id: string, newLocalStream: MediaStream | null): void => {
    mediaStreams.value[id].localStream = newLocalStream;
  };

  const setRemoteStream = (id: string, newRemoteStream: MediaStream | null): void => {
    mediaStreams.value[id].remoteStream = newRemoteStream;
  };

  const setPeerConnection = (id: string, newPeerConnection: RTCPeerConnection | null): void => {
    mediaStreams.value[id].peerConnection = newPeerConnection;
  };

  const setConnectionState = (id: string, newPeerConnectionState: PeerConnectionState): void => {
    mediaStreams.value[id].connectionState = newPeerConnectionState;
  };

  const setLocalVideo = (id: string, newVideo: boolean): void => {
    mediaStreams.value[id].localOptions.video = newVideo;
  };

  const setLocalAudio = (id: string, newAudio: boolean): void => {
    mediaStreams.value[id].localOptions.audio = newAudio;
  };

  const setRemoteVideo = (id: string, newVideo: boolean): void => {
    mediaStreams.value[id].remoteOptions.video = newVideo;
  };

  const setRemoteAudio = (id: string, newAudio: boolean): void => {
    mediaStreams.value[id].remoteOptions.audio = newAudio;
  };

  const setRtcDescription = (id: string, newDescription: RTCSessionDescription | null): void => {
    mediaStreams.value[id].rtcDescription = newDescription;
  };

  const getConnectionByChatId = (id: string) => mediaStreams.value[id];

  const onChangeConnectionState = (id = '') => {
    const connectionState = getConnectionByChatId(id).peerConnection?.connectionState;

    if (connectionState) {
      setConnectionState(id, connectionState);
    }
  };

  const watchForPeerConnectionState = (id: string, newPeerConnection: RTCPeerConnection | null) => {
    newPeerConnection?.addEventListener('connectionstatechange', onChangeConnectionState.bind(null, id));
  };

  const unwatchForPeerConnectionState = (newPeerConnection: RTCPeerConnection | null) => {
    newPeerConnection?.removeEventListener('connectionstatechange', onChangeConnectionState.bind(null, ''));
  };

  const isExistOffer = (id: string) => getConnectionByChatId(id)?.peerConnection === null;

  const reset = (chatId: string) => {
    unwatchForPeerConnectionState(getConnectionByChatId(chatId)?.peerConnection);
    delete mediaStreams.value[chatId];
  };

  const getLocalStream = (id: string) => computed(() => getConnectionByChatId(id)?.localStream);
  const getRemoteStream = (id: string) => computed(() => getConnectionByChatId(id)?.remoteStream);
  const getRemoteOptions = (id: string) => computed(() => getConnectionByChatId(id)?.remoteOptions);
  const getLocalOptions = (id: string) => computed(() => getConnectionByChatId(id)?.localOptions);
  const getPeerConnection = (id: string) => computed(() => getConnectionByChatId(id)?.peerConnection);
  const getConnectionState = (id: string) => computed(() => getConnectionByChatId(id)?.connectionState);
  const getRtcDescription = (id: string) => computed(() => getConnectionByChatId(id)?.rtcDescription);

  return {
    reset,
    isExistOffer,
    setLocalAudio,
    setLocalVideo,
    setRemoteVideo,
    setRemoteAudio,
    setLocalStream,
    setRemoteStream,
    setPeerConnection,
    setRtcDescription,
    setConnectionState,
    getConnectionByChatId,
    initStreamOptionsStructure,
    watchForPeerConnectionState,
    unwatchForPeerConnectionState,
    getLocalStream,
    getRemoteStream,
    getRemoteOptions,
    getLocalOptions,
    getPeerConnection,
    getConnectionState,
    getRtcDescription,
  };
});
