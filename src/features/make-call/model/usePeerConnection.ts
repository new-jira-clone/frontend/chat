import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { useMediaStreamStore } from './useMediaStreamStore';

export function usePeerConnection() {
  const { socket } = useSocketStore();
  const { reset: resetMediaStream } = useMediaStreamStore();
  const { setRemoteStream } = useMediaStreamStore();

  const createPeerConnection = (chatId: string) => {
    const { getLocalStream } = useMediaStreamStore();

    const localStream = getLocalStream(chatId);

    const connection = new RTCPeerConnection({
      // iceServers: [{ urls: 'stun:stun2.1.google.com:19302' }],
    });

    localStream.value?.getTracks().forEach((track) => {
      if (localStream.value) {
        connection.addTrack(track, localStream.value);
      }
    });

    connection.addEventListener('icecandidate', ({ candidate }) => {
      if (!candidate) return;
      socket.value?.emit('sendCandidate', {
        chatId,
        candidate,
      });
    });

    connection.addEventListener('track', ({ streams }) => {
      const videoStream = streams[0];

      if (videoStream) {
        setRemoteStream(chatId, streams[0]);
      }
    });

    return connection;
  };

  const cancelConnection = (chatId: string) => {
    const { getLocalStream, getPeerConnection } = useMediaStreamStore();

    getLocalStream(chatId).value?.getTracks().forEach((mediaStream) => mediaStream.stop());
    getPeerConnection(chatId).value?.close();
    resetMediaStream(chatId);
  };

  return {
    createPeerConnection,
    cancelConnection,
  };
}
