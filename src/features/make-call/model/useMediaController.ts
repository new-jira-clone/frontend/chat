import { useSocketStore } from '@new-jira-clone/libs/src/store';
import { useMediaStreamStore } from './useMediaStreamStore';

type VideoSettings = boolean | {
  width: number;
  height: number;
};

export default () => {
  // eslint-disable-next-line arrow-body-style
  const getMediaStream = async (videoSettings: VideoSettings = { width: 1280, height: 720 }) => {
    return navigator.mediaDevices.getUserMedia({ audio: true, video: videoSettings });
  };

  const toggleVideo = async (connectionTargetId: string): Promise<void> => {
    const { socket } = useSocketStore();
    const {
      setLocalVideo,
      setLocalStream,
      getLocalStream,
      getLocalOptions,
      getPeerConnection,
    } = useMediaStreamStore();

    const localStream = getLocalStream(connectionTargetId);
    const localOptions = getLocalOptions(connectionTargetId);
    const peerConnection = getPeerConnection(connectionTargetId);

    // TODO: add info about permission denied
    if (localStream.value === null) return;

    const enableVideoTracks = localStream.value?.getVideoTracks();
    const toggledVideo = !localOptions.value.video;

    const emitToggledVideo = (videoStatus: boolean) => {
      socket.value?.emit('toggleVideo', {
        chatId: connectionTargetId,
        video: videoStatus,
      });
    };

    if (enableVideoTracks?.length) {
      enableVideoTracks.forEach((videoTrack) => {
        if (videoTrack.enabled) {
          // eslint-disable-next-line no-param-reassign
          videoTrack.enabled = false;
        } else {
          // eslint-disable-next-line no-param-reassign
          videoTrack.enabled = true;
        }
      });

      emitToggledVideo(toggledVideo);
      setLocalVideo(connectionTargetId, toggledVideo);

      return;
    }

    const mediaStream = await getMediaStream();

    mediaStream.getTracks().forEach((track) => {
      if (track.kind === 'audio') {
        // eslint-disable-next-line no-param-reassign
        track.enabled = localOptions.value.audio;
      }

      peerConnection.value?.addTrack(track, mediaStream);
    });

    emitToggledVideo(toggledVideo);
    setLocalStream(connectionTargetId, mediaStream);
    setLocalVideo(connectionTargetId, toggledVideo);
  };

  const toggleAudio = async (connectionTargetId: string): Promise<void> => {
    const { setLocalAudio, getLocalOptions, getLocalStream } = useMediaStreamStore();
    const { socket } = useSocketStore();

    // TODO: add info about permission denied
    if (getLocalStream(connectionTargetId).value === null) return;

    const toggledAudio = !getLocalOptions(connectionTargetId).value.audio;

    getLocalStream(connectionTargetId).value?.getAudioTracks().forEach((audioTrack) => {
      if (audioTrack.enabled) {
        // eslint-disable-next-line no-param-reassign
        audioTrack.enabled = false;
      } else {
        // eslint-disable-next-line no-param-reassign
        audioTrack.enabled = true;
      }
    });

    socket.value?.emit('toggleAudio', {
      chatId: connectionTargetId,
      audio: toggledAudio,
    });

    setLocalAudio(connectionTargetId, toggledAudio);
  };

  return {
    getMediaStream,
    toggleAudio,
    toggleVideo,
  };
};
