module.exports = {
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  'editor.formatOnSave': true,
  trailingComma: 'es5',
  bracketSpacing: true,
  printWidth: 130,
  endOfLine: 'auto',
  singleAttributePerLine: true,
};
