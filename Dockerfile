FROM node:18-alpine AS build-stage
WORKDIR /chat
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
FROM nginx:alpine as production-stage
COPY --from=build-stage /chat/dist /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf 
EXPOSE 85
CMD ["nginx", "-g", "daemon off;"]
