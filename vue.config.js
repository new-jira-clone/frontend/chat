const path = require('path');
const { defineConfig } = require('@vue/cli-service');
// eslint-disable-next-line import/no-extraneous-dependencies
const { ModuleFederationPlugin } = require('webpack').container;
const dev = require('./package.json');

module.exports = defineConfig({
  publicPath: process.env.VUE_APP_CHAT,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @use "./node_modules/@new-jira-clone/ui/src/styles/tools/index.scss" as tools;
          `,
      },
    },
  },
  transpileDependencies: true,
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
        '@tools': path.resolve(__dirname, './node_modules/@new-jira-clone/ui/src/styles/tools/index.scss'),
      },
    },
    optimization: {
      splitChunks: false,
    },
    entry: path.resolve(__dirname, './src/index.ts'),
    plugins: [
      new ModuleFederationPlugin({
        name: 'Chat',
        filename: 'remoteEntry.js',
        exposes: {
          './router': './src/app/router/index.ts',
        },
        shared: {
          ...dev.dependencies,
          '@new-jira-clone/libs/': {
            eager: false,
            singleton: true,
            strictVersion: false,
            requiredVersion: dev.dependencies['@new-jira-clone/libs'],
          },
          '@new-jira-clone/ui/': {
            eager: false,
            singleton: true,
            strictVersion: false,
            requiredVersion: dev.dependencies['@new-jira-clone/ui'],
          },
        },
      }),
    ],
    devServer: {
      port: 3005,
    },
  },
});
