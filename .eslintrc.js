module.exports = {
  root: true,
  env: {
    node: true,
    'vue/setup-compiler-macros': true,
  },
  extends: ['plugin:vue/vue3-essential', '@vue/airbnb', '@vue/typescript/recommended'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'import/prefer-default-export': 'off',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'linebreak-style': 'off',
    camelcase: 'off',
    // 'implicit-arrow-linebreak': 'off',
    'func-call-spacing': 'off',
    'no-spaced-func': 'off',
    'vuejs-accessibility/form-control-has-label': 'off',
    'no-shadow': 'off',
    'prefer-destructuring': 'off',
    '@typescript-eslint/no-shadow': 'error',
    'operator-linebreak': [1, 'before'],
    'max-len': ['error', { code: 120 }],
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
    {
      files: ['./src/pages/*'],
      rules: {
        'vue/multi-word-component-names': 'off',
      },
    },
  ],
};
